python-byteplay (0.2-3) UNRELEASED; urgency=low

  [ Jakub Wilk ]
  * Rename the DEP-8 test, so that it matches binary package name.
  * Remove doubled shebang from the DEP-8 test script.
  * Use canonical URIs for Vcs-* fields.
  * Don't use dh_testdir; instead, use makefile rules to ensure that
    debian/rules can be only run in the correct directory.
  * Bump standards version to 3.9.4 (no changes needed).

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/control: Remove trailing whitespaces
  * d/control: Remove ancient XS-Python-Version field
  * Convert git repository from git-dpm to gbp layout
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 17:03:46 -0500

python-byteplay (0.2-2) unstable; urgency=low

  * Bump standards version to 3.9.3.
    + Update debian/copyright URI.
  * Export PYTHONWARNINGS=d in debian/rules to enable all warnings in Python
    code.
  * Make binary(-indep) target depend on build stamp file rather than build
    target.
  * Don't pass control file path to pyversions explicitly.
  * Remove ‘zip_safe = True’ from setup.py; this is a setuptools-specific
    option, but in Debian we use pure distutils.
  * Add “<< 2.8” to XS-Python-Version. The package requires porting to each
    Python version separately.
  * Test more comprehensively: try to re-bytecopile whole Python standard
    library.
  * Add DEP-8 tests.

 -- Jakub Wilk <jwilk@debian.org>  Mon, 23 Apr 2012 15:00:23 +0200

python-byteplay (0.2-1) unstable; urgency=low

  * Initial release (closes: #660745).

 -- Jakub Wilk <jwilk@debian.org>  Wed, 22 Feb 2012 20:30:46 +0100
