#!/usr/bin/python

import os
import re
import shutil
import sys
import tempfile

import byteplay


def main():
    print >>sys.stderr, 'byteplay.__file__ = %r' % (byteplay.__file__,)
    source_dir = os.path.dirname(os.__file__)
    os.chdir(source_dir)
    target_dir = tempfile.mkdtemp(prefix='byteplay-test')
    is_name_interesting = re.compile('^\w+(?:[.]py)?$').match
    try:
        def ignore(root, names):
            root = root[2:]
            return [name for name in names if not is_name_interesting(name)]
        shutil.copytree('.', os.path.join(target_dir, 'Lib'), ignore=ignore)
        sys.argv[1:] = [target_dir]
        byteplay.main()
    finally:
        shutil.rmtree(target_dir)


if __name__ == '__main__':
    main()

# vim:ts=4 sw=4 et
